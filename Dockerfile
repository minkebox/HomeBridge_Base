FROM alpine:3.12

COPY root/ /

RUN apk add nodejs nodejs-npm dbus avahi avahi-compat-libdns_sd git curl ;\
    cd /app ; npm install ;\
    rm -f /etc/avahi/services/*

EXPOSE 51826

VOLUME [ "/app/homebridge" ]

ENV BRIDGE_USERNAME 00:00:00:00:00:00
ENV BRIDGE_PIN 111-11-111
ENV BRIDGE_PORT 51826

HEALTHCHECK --interval=60s --timeout=5s CMD /health.sh

ENTRYPOINT ["/startup.sh"]
