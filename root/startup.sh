#! /bin/sh

HOMEBRIDGE_FLAGS=

if [ -x /app/configure.sh ]; then
  . /app/configure.sh
fi
if [ "${DEBUG}" != "" ]; then
  HOMEBRIDGE_FLAGS="${HOMEBRIDGE_FLAGS} -D"
fi

trap "killall -2 homebridge ; sleep 10 ; killall dbus-daemon avahi-daemon node homebridge sleep; exit" TERM INT

dbus-daemon --system
avahi-daemon -D

/app/node_modules/homebridge/bin/homebridge --user-storage-path /app/homebridge --plugin-path /app/plugins $HOMEBRIDGE_FLAGS &

sleep 2147483647d &
wait "$!"
