#! /bin/sh

# Homebridge is running
if [ "$(pidof homebridge)" = "" ]; then
  exit 1
fi

# Webserver is running
if [ "$(curl -Is localhost:${BRIDGE_PORT} > /dev/null && echo 'okay')" = "" ]; then
  exit 1
fi

exit 0
